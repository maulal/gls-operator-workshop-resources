= Chapter 1, Exercise 1.4.1 Howto deploy a simple workload
Rik Megens <rmegens@redhat.com>; Mohamed Zaher <mzaher@redhat.com>
v1.01, 2020-05-14
:revnumber: 2.01
:toc:
:toclevels: 5
:revdate: 17-11-2020
:revremark: Openshift Operator Workshop By Red Hat, GLS EMEA
:version-label!:
:organization: Red Hat, GLS EMEA
:pdf-theme: base
:pdf-themesdir: resources/themes/
:title-logo-image: images/Logo-RedHat-B-Red-RGB.svg

== How to Setup a simple workload

To solve this exercise, you can make use of the S2I functionality in Openshift.
After creating a project, with S2I, you can create the new application by simply
pointing to git repo and tell S2I you need the base runtime for httpd containers.

After creating the resources, to make the app available from outside of the cluster,
a route resource needs to be created by exposing the service resource.

====
WARNING:: It can happen that the solution needs some tweaking to the the app to work. We expect a participant to
do this type of tweaking since it is part of the workshop prerequisites.
====

=== Steps

[source, text]
----
# make sure you are logged in with an account that can create projects, like the
# developer account
oc new-project [your-projectname-here]
oc new-app --name my-webapp \
httpd~https://gitlab.com/[your-gitlab-account/gls-operator-workshop-resources \
--context-dir=awesome-app

# check out if all resources are created
oc get all

# create the route after the app pod has been deployed
oc expose svc my-webapp

# test your app from the workstation
# grab the url
oc get route my-webapp

# using curl to show the page
curl http://[your-webapp-external-url]
----

=== What can go wrong ?

Since this is a very simple example, not much can go wrong however, watch out for:

.Known issues
* issues with pulling from quay.io
* issues with container image generation

The  '_oc logs_' and '_oc events_' commands should give you insights in and help you solve the issues.